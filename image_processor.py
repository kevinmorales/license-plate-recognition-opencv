import cv2
import imutils
import numpy as np
from PIL import Image


class Processor:
    def __init__(self):
        pass

    def get_image(self, image_file):
        img = cv2.imread(image_file, cv2.IMREAD_COLOR)
        cv2.imshow("before blurring", img)
        return img

    def blur_image(self, image):
        image = cv2.GaussianBlur(image, (7, 7), 0)
        cv2.imshow("after blurring", image)
        imutils.resize(image, width=505)
        return image

    def to_gray_scale(self, image):
        # Convert to gray scale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # Bilateral Filter smooths noisy images while preserving edges
        gray = cv2.bilateralFilter(gray, 11, 17, 17)
        # Display gray scaled image
        cv2.imshow("Gray scaled", gray)
        return gray

    def detect_edges(self, image):
        # Perform Edge detection with Canny function
        edge_detected = cv2.Canny(image, 32, 210)
        cv2.imshow("Edges Detected", edge_detected)
        return edge_detected

    def contour(self, image):
        return cv2.findContours(image.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    def trace_contours(self, image, contours):
        img1 = image.copy()
        cv2.drawContours(img1, contours, -1, (0, 247, 0), 3)
        cv2.imshow("img1", img1)
        return img1

    def sort_contours(self, cont):
        cont = sorted(cont, key=cv2.contourArea, reverse=True)[:30]
        return cont

    def process_with_contours(self, contours, img, name, cropped_directory):
        number_plate_contour = None
        count = 0
        # loop over contours
        for c in contours:
            # approximate the contour
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.018 * peri, True)
            # Chooses the contours with 4 corners
            if len(approx) == 4:
                number_plate_contour = approx
                # Finds coordinates of the plate
                x, y, w, h = cv2.boundingRect(c)
                new_img = img[y : y + h, x : x + w]
                cv2.imwrite(
                    "./" + cropped_directory + "/" + name[: len(name) - 4] + ".png",
                    new_img,
                )
                break
            # draws the selected contour on original image
        cv2.drawContours(img, [number_plate_contour], -1, (0, 255, 0), 3)
        cv2.imshow("Final image with plate detected", img)
        Cropped_loc = "./" + cropped_directory + "/" + name[: len(name) - 4] + ".png"
        cv2.imshow("cropped", cv2.imread(Cropped_loc))
        return Cropped_loc
