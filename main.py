import os
import cv2
import sys
import pytesseract as tess
from image_processor import Processor


tess.pytesseract.tesseract_cmd = r"C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

processor = Processor()

cropped_dir = "cropped"


def main():

    # Get the directory from the user
    directory = sys.argv[1]

    actual_file = sys.argv[2]

    # Get the current working directory
    cwd = os.getcwd()

    # Form the path to the files
    path = str(cwd) + "/" + str(directory) + "/"

    with open(str(cwd) + "/" + actual_file) as a:
        lines = a.read().splitlines()

    # For each file in the directory
    for image_file in os.listdir(directory):

        # Get image to analyze
        my_image = processor.get_image(path + image_file)

        # Blur the image to remove unnecessary noise
        my_image = processor.blur_image(my_image)

        # Gray scale the image
        gray_image = processor.to_gray_scale(my_image)

        # Detect the edges in the image
        edge_detected_image = processor.detect_edges(gray_image)

        # Get the contours from the edge detected image
        contours, new = processor.contour(edge_detected_image)

        # Draw the contours of the image
        img1 = processor.trace_contours(my_image, contours)

        # Sort the contours list
        contours = processor.sort_contours(contours)

        # Draw the contours from the newly sorted contours list
        img2 = processor.trace_contours(my_image, contours)

        # Process the image and get the cropped license plate
        cropped_loc = processor.process_with_contours(
            contours, my_image, image_file, cropped_dir
        )

        # Turn the final image into a string
        plate_to_text = tess.image_to_string(cropped_loc, lang="eng")

        # Print the plate characters detected
        print(
            "License Plate Number Detected is: ",
            plate_to_text[: len(plate_to_text) - 2],
        )

        # Print the actual plate numbers
        print("License Plate Number Expected is: ", lines[0])

        # IF the expected and detected are the same
        if plate_to_text[: len(plate_to_text) - 2] == lines[0]:
            # Its a match
            print("MATCH")
        # ELSE
        else:
            # Not a match
            print("NO MATCH")

        # Remove the plate number expected
        lines.remove(lines[0])

        # Wait for the user to proceed to next image
        cv2.waitKey(0)


main()
